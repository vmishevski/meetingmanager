﻿using MeetingManager.DataSource.DbModel;
using MeetingManager.DataSource.Repository;
using StructureMap;

namespace MeetingManager.DataSource.DependencyInjection
{
    public static class DataSourceIoC
    {
        public static void Bootstrap(IInitializationExpression initializationExpression)
        {
            initializationExpression.For(typeof (IMeetingRepository<>)).Use(typeof (MeetingRepository<>));

            // Containers are created per request. So return same context for whole request.
            initializationExpression.For<MeetingManagerDbContext>().Singleton().Use(() => new MeetingManagerDbContext());
        }
    }
}
