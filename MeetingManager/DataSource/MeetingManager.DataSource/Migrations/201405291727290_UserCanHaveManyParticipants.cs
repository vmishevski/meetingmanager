namespace MeetingManager.DataSource.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserCanHaveManyParticipants : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "Meeting_Id", "dbo.Meetings");
            DropIndex("dbo.Users", new[] { "Meeting_Id" });
            CreateTable(
                "dbo.MeetingUsers",
                c => new
                    {
                        Meeting_Id = c.Int(nullable: false),
                        User_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Meeting_Id, t.User_Id })
                .ForeignKey("dbo.Meetings", t => t.Meeting_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Meeting_Id)
                .Index(t => t.User_Id);
            
            DropColumn("dbo.Users", "Meeting_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Meeting_Id", c => c.Int());
            DropForeignKey("dbo.MeetingUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.MeetingUsers", "Meeting_Id", "dbo.Meetings");
            DropIndex("dbo.MeetingUsers", new[] { "User_Id" });
            DropIndex("dbo.MeetingUsers", new[] { "Meeting_Id" });
            DropTable("dbo.MeetingUsers");
            CreateIndex("dbo.Users", "Meeting_Id");
            AddForeignKey("dbo.Users", "Meeting_Id", "dbo.Meetings", "Id");
        }
    }
}
