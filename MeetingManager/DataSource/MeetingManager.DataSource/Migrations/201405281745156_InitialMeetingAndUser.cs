namespace MeetingManager.DataSource.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMeetingAndUser : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Meetings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(),
                        Description = c.String(),
                        MeetingTime = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        UpdateDateTime = c.DateTime(nullable: false),
                        Creator_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Creator_Id)
                .Index(t => t.Creator_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UpdateDateTime = c.DateTime(nullable: false),
                        Meeting_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Meetings", t => t.Meeting_Id)
                .Index(t => t.Meeting_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Meeting_Id", "dbo.Meetings");
            DropForeignKey("dbo.Meetings", "Creator_Id", "dbo.Users");
            DropIndex("dbo.Users", new[] { "Meeting_Id" });
            DropIndex("dbo.Meetings", new[] { "Creator_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.Meetings");
        }
    }
}
