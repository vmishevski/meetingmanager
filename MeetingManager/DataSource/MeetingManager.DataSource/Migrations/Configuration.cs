using MeetingManager.Common.EntityModel;
using MeetingManager.DataSource.DbModel;
using System.Data.Entity.Migrations;

namespace MeetingManager.DataSource.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<MeetingManagerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MeetingManagerDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Users.AddOrUpdate(new User()
                              {
                                  Name = "Voislav"
                              });
            context.Users.AddOrUpdate(new User()
                              {
                                  Name = "Katerina"
                              });

            context.Users.AddOrUpdate(new User()
                              {
                                  Name = "Ivica"
                              });
            context.SaveChanges();
        }
    }
}
