﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MeetingManager.Common.EntityModel.Base;
using MeetingManager.DataSource.DbModel;

namespace MeetingManager.DataSource.Repository
{
    public class MeetingRepository<T> : IMeetingRepository<T> where T: BaseEntity
    {
        private readonly MeetingManagerDbContext _context;

        public MeetingRepository(MeetingManagerDbContext context)
        {
            _context = context;
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public List<T> GetList()
        {
            return _context.Set<T>().ToList();
        }

        public List<T> GetList(params Expression<Func<T, bool>>[] filters)
        {
            var entities = _context.Set<T>().AsQueryable();

            foreach (var filter in filters)
            {
                entities = entities.Where(filter);
            }

            return entities.ToList();
        }


        public void Save(T entity)
        {
            var existingEntity = GetById(entity.Id);
            

            if (existingEntity != null)
            {
                _context.Entry(existingEntity).State = EntityState.Modified;
            }
            else
            {
                _context.Set<T>().Add(entity);
            }
        }

        public void Remove(int id)
        {           
            var existing = _context.Set<T>().Find(id);

            if (existing != null)
            {
                _context.Set<T>().Remove(existing);
            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
