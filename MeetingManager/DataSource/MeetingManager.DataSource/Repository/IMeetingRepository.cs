﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MeetingManager.Common.EntityModel.Base;

namespace MeetingManager.DataSource.Repository
{
    public interface IMeetingRepository<T> where T: BaseEntity
    {
        /// <summary>
        /// Retrieves single entity by it's Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(int id);

        /// <summary>
        /// Retrieves all entities in the database
        /// </summary>
        /// <returns></returns>
        List<T> GetList();

        /// <summary>
        /// Returns list of <see cref="T"/> which satisfy all the filtering predicates provided
        /// </summary>
        /// <param name="filters">List of predicates</param>
        /// <returns></returns>
        List<T> GetList(params Expression<Func<T, bool>>[] filters); 

        /// <summary>
        /// Save single entity in database
        /// Checks if it exists or else adds the entity
        /// </summary>
        /// <param name="entity"></param>
        void Save(T entity);

        /// <summary>
        /// Removes single entity by it's Id
        /// </summary>
        /// <param name="id"></param>
        void Remove(int id);

        /// <summary>
        /// Commits all changes to the database
        /// </summary>
        void SaveChanges();
    }
}
