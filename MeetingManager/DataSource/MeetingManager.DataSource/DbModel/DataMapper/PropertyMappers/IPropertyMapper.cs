namespace MeetingManager.DataSource.DbModel.DataMapper.PropertyMappers
{
    interface IPropertyMapper
    {
        string GetSql();
    }
}