using System.Reflection;

namespace MeetingManager.DataSource.DbModel.DataMapper.PropertyMappers
{
    public class DefaultPropertyMapper : IPropertyMapper
    {
        private readonly PropertyInfo _property;

        public DefaultPropertyMapper(PropertyInfo property)
        {
            _property = property;
        }

        public string GetSql()
        {
            return _property.Name;
        }
    }
}