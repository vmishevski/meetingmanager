using System;

namespace MeetingManager.DataSource.DbModel.DataMapper.PropertyMappers
{
    public class PropertyValueMapper : IPropertyMapper
    {
        private readonly object _value;

        public PropertyValueMapper(object value)
        {
            if (!value.GetType().IsValueType)
            {
                throw new Exception("Property must be mapped to value type");
            }

            _value = value;
        }

        public string GetSql()
        {
            return "'" + _value.ToString() + "'";
        }
    }
}