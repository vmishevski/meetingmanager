namespace MeetingManager.DataSource.DbModel.DataMapper.PropertyMappers
{
    public class ColumnNameMapper : IPropertyMapper
    {
        private readonly string _columnName;
        public ColumnNameMapper(string columnName)
        {
            _columnName = columnName;
        }

        public string GetSql()
        {
            return _columnName;
        }
    }
}