﻿using System;
using System.Data.Entity;
using log4net;
using MeetingManager.Common.EntityModel;

namespace MeetingManager.DataSource.DbModel
{
    public class MeetingManagerDbContext : DbContext
    {
        private readonly Guid _contextGuid;

        private readonly ILog _logger = LogManager.GetLogger("SqlQueryLogger");

        static MeetingManagerDbContext()
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
        }

        private const string ConnectionString = "MeetingManagerDbContext";

        public DbSet<Meeting> Meetings { get; set; }

        public DbSet<User> Users { get; set; }
 
        public MeetingManagerDbContext() : base(ConnectionString)
        {
            _contextGuid = Guid.NewGuid();
            ILog log = LogManager.GetLogger(this.GetType());
            log.Debug("Context guid:" + _contextGuid);
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;

            Database.Log = LogSqlQuery;
        }

        private void LogSqlQuery(string s)
        {
            _logger.Debug(s);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Meeting>().HasMany(m => m.Participants).WithMany();
            
        }
    }
}
