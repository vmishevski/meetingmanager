﻿namespace MeetingManager.DataSource.DbModel
{
    public class Entity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual TestForeignKey FKey { get; set; }

    }

    public class TestForeignKey
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }


}