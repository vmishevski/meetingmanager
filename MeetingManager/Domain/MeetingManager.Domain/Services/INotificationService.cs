﻿namespace MeetingManager.Domain.Services
{
    public interface INotificationService
    {
        void NotifyNewMeetingCreated(int meetingId);
    }
}
