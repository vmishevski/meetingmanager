﻿using System;
using System.Collections.Generic;
using System.Linq;
using MeetingManager.Common.EntityModel;
using MeetingManager.DataSource.Repository;
using MeetingManager.Domain.Commands;

namespace MeetingManager.Domain.Services
{
    public class MeetingService : IMeetingService
    {
        private readonly IMeetingRepository<Meeting> _meetingRepository;
        private readonly IMeetingRepository<User> _userRepository; 

        public MeetingService(IMeetingRepository<Meeting> meetingRepository, IMeetingRepository<User> userRepository)
        {
            _meetingRepository = meetingRepository;
            _userRepository = userRepository;
        }

        public Meeting SaveMeeting(Meeting meeting)
        {
            var command = new CommandInTransaction<Meeting>(
                () =>
                {
                    if (meeting.Participants.Count < 2)
                    {
                        throw new Exception("Meeting must have two participants");
                    }

                    var existing = _meetingRepository.GetById(meeting.Id);

                    if (existing == null)
                    {
                        _meetingRepository.Save(meeting);
                        _meetingRepository.SaveChanges();
                        return meeting;
                    }
                    else
                    {
                        existing.Description = meeting.Description;
                        existing.Subject = meeting.Subject;
                        existing.MeetingTime = meeting.MeetingTime;

                        var modelArray = meeting.Participants.Select(user => user.Id).ToList();
                        var entityArray = existing.Participants.Select(user => user.Id).ToList();

                        var toRemove = entityArray.Where(i => !modelArray.Contains(i)).ToList();
                        var toAdd = modelArray.Where(i => !entityArray.Contains(i)).Select(i => _userRepository.GetById(i)).ToList();
                        var count = existing.Participants.Count;
                        var participants = existing.Participants.ToArray(); 
                        for (int index = count-1; index >= 0; index--)
                        {
                            var p = participants[index];
                            if (toRemove.Contains(p.Id))
                            {
                                existing.Participants.Remove(p);
                            }
                        }

                        foreach (var user in toAdd)
                        {
                            existing.Participants.Add(user);
                        }

                        _meetingRepository.Save(existing);
                        _meetingRepository.SaveChanges();
                        return existing;
                    }
                });

            return command.Execute();

        }

        public void RemoveMeeting(int meetingId)
        {
            var command = new CommandInTransaction(
                () =>
                {
                    var meeting = _meetingRepository.GetById(meetingId);
                    if (meeting != null)
                    {
                        meeting.Active = false;
                        _meetingRepository.Save(meeting);
                        _meetingRepository.SaveChanges();
                    }
                });

            command.Execute();
        }

        public List<Meeting>  GetAllMeetings()
        {
            var command = new CommandInTransaction<List<Meeting>>(() =>
                _meetingRepository.GetList(meeting => meeting.Active)
                );

            return command.Execute();
        }

        public Meeting GetMeetingById(int id)
        {
            var command = new CommandInTransaction<Meeting>(() => _meetingRepository.GetById(id));

            return command.Execute();
        }

        public User AddUser(string userName)
        {
            var command = new CommandInTransaction<User>(
                () =>
                {
                    var exists = _userRepository.GetList(user => user.Name == userName).FirstOrDefault();

                    if (exists != null) return exists;

                    var newUser = new User()
                               {
                                   Name = userName,
                                   UpdateDateTime = DateTime.Now
                               };

                    _userRepository.Save(newUser);
                    _userRepository.SaveChanges();

                    return newUser;
                });

            return command.Execute();
        }

        public IEnumerable<User> GetAllUsers()
        {
            var command = new CommandInTransaction<IEnumerable<User>>(() => _userRepository.GetList());

            return command.Execute();
        }

        public User GetUserById(int id)
        {
            var command = new CommandInTransaction<User>(() => _userRepository.GetById(id));

            return command.Execute();
        }

        public List<int> GetMeetingsForUser(string userName)
        {
            var command = new CommandInTransaction<List<int>>(
                () => _meetingRepository.GetList()
                    .Where(meeting => meeting.Participants.Any(user => user.Name == userName))
                    .Select(meeting => meeting.Id)
                    .ToList());

            return command.Execute();
        }
    }
}
