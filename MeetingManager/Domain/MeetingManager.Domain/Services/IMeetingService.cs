﻿using System.Collections.Generic;
using MeetingManager.Common.EntityModel;

namespace MeetingManager.Domain.Services
{
    public interface IMeetingService
    {
        /// <summary>
        /// Persist all changes on a <see cref="Meeting"/> in database. Synchronezes all the changes for the 
        /// <see cref="Meeting"/> properties.
        /// Adds/removes users accordingly
        /// </summary>
        /// <param name="meeting">Meeting entity to persist</param>
        /// <returns></returns>
        Meeting SaveMeeting(Meeting meeting);

        /// <summary>
        /// Removes <see cref="Meeting"/> by it's Id
        /// </summary>
        /// <param name="meetingId">Id of the <see cref="Meeting"/> to delete</param>
        void RemoveMeeting(int meetingId);

        /// <summary>
        /// Retrieves all meetings from database
        /// </summary>
        /// <returns></returns>
        List<Meeting> GetAllMeetings();

        /// <summary>
        /// Retrieves single <see cref="Meeting"/> by it's Id 
        /// </summary>
        /// <param name="id">Id of the meeting to retrieve</param>
        /// <returns><see cref="Meeting"/> with the given Id</returns>
        /// <remarks>Throws exception if meeting not found</remarks>
        Meeting GetMeetingById(int id);

        /// <summary>
        /// Adds user to database
        /// </summary>
        /// <param name="userName">Username of the user</param>
        /// <returns>Persisted <see cref="User"/> entity</returns>
        User AddUser(string userName);

        /// <summary>
        /// Retrieves all users from database
        /// </summary>
        /// <returns>List of <see cref="User"/> entities</returns>
        IEnumerable<User> GetAllUsers();

        /// <summary>
        /// Retrieves user by it's id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        User GetUserById(int id);

        /// <summary>
        /// For a given user, retrieves all meetings, past and future, in which he belongs
        /// </summary>
        /// <param name="userName">Username of the user</param>
        /// <returns>List of ids of all meetings which the user participates</returns>
        List<int> GetMeetingsForUser(string userName);
    }
}
