﻿using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MeetingManager.Domain.Services;
using Microsoft.AspNet.SignalR;
using StructureMap;

namespace MeetingManager.Domain.Notifications
{
    /// <summary>
    /// SignalR hub used for sending messages on participants of meetings
    /// </summary>
    public class NotificationHub : Hub
    {
        /// <summary>
        /// Notify all clients in the group specified by the meetingId paramether
        /// </summary>
        /// <param name="meetingId"></param>
        /// <param name="message"></param>
        public void SendNotification(int meetingId, string message)
        {
            Clients.OthersInGroup(meetingId.ToString(CultureInfo.InvariantCulture)).displayMessage(message);
        }

        /// <summary>
        /// After connecting to the hub, join all necessary groups
        /// Groups are identified by meeting in which the user participates
        /// </summary>
        /// <param name="userName"></param>
        public void JoinGroups(string userName)
        {
            // Add the connection id in memory to keep the userName for that connection
            ConnectionDictionary.AddConnection(userName, Context.ConnectionId);

            var service = ObjectFactory.GetInstance<IMeetingService>();

            // get all meetings for this user
            var meetingIds = service.GetMeetingsForUser(userName);

            foreach (var meetingId in meetingIds)
            {
                // join the groups identified by the meeting ids
                Groups.Add(Context.ConnectionId, meetingId.ToString(CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        /// When disconected, remove the connection from memory storage
        /// </summary>
        /// <returns></returns>
        public override Task OnDisconnected()
        {
            ConnectionDictionary.RemoveConnection(Context.ConnectionId);

            return base.OnDisconnected();
        }

        /// <summary>
        /// When new meeting is created, add all it's participants the same meeting group
        /// </summary>
        /// <param name="meetingId"></param>
        public void OnNewGroupCreated(int meetingId)
        {
            var service = ObjectFactory.GetInstance<IMeetingService>();

            var meeting = service.GetMeetingById(meetingId);

            foreach (var userName in meeting.Participants.Select(user => user.Name).ToList())
            {
                if (ConnectionDictionary.ContainsConnection(userName))
                {
                    var connectionId = ConnectionDictionary.GetConnection(userName);
                    Groups.Add(connectionId, meetingId.ToString(CultureInfo.InvariantCulture));
                }
            }
        }
    }
}
