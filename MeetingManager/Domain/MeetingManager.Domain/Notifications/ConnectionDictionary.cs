﻿using System;
using System.Collections.Generic;
using MeetingManager.Common.EntityModel;

namespace MeetingManager.Domain.Notifications
{
    /// <summary>
    /// Singletor dictonary for mapping users to connections
    /// </summary>
    public static class ConnectionDictionary
    {
        private static readonly Lazy<Dictionary<string, string>> _dictionary = new Lazy<Dictionary<string, string>>(() => new Dictionary<string, string>());

        private static Dictionary<string, string> Dictionary
        {
            get { return _dictionary.Value; }
        }

        public static void AddConnection(string userName, string connectionId)
        {
            if (!Dictionary.ContainsKey(userName))
                Dictionary.Add(userName, connectionId);
            else
                Dictionary[userName] = connectionId;
        }

        public static void RemoveConnection(string connectionId)
        {
            Dictionary.Remove(connectionId);
        }

        public static bool ContainsConnection(string userName)
        {
            return Dictionary.ContainsKey(userName);
        }

        public static string GetConnection(string userName)
        {
            return Dictionary[userName];
        }
    }
}
