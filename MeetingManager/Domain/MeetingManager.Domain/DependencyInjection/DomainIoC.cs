﻿using MeetingManager.DataSource.DependencyInjection;
using MeetingManager.Domain.Services;
using StructureMap;

namespace MeetingManager.Domain.DependencyInjection
{
    public static class DomainIoC
    {
        public static void Bootstrap(IInitializationExpression initializationExpression)
        {
            initializationExpression.For<IMeetingService>().Use<MeetingService>();

            DataSourceIoC.Bootstrap(initializationExpression);
        }
    }
}
