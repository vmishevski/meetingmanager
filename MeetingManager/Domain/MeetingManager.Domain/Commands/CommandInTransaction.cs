using System;
using System.Transactions;

namespace MeetingManager.Domain.Commands
{
    public class CommandInTransaction
    {
        public Action Action { get; set; }
        public CommandInTransaction(Action actionToExecute)
        {
            Action = actionToExecute;
        }

        public void Execute()
        {
            using (var transactionScope = new TransactionScope())
            {
                try
                {
                    Action();
                    transactionScope.Complete();
                }
                catch (Exception)
                {
                    transactionScope.Dispose();
                    throw;
                }
            }
        }
    }

    public class CommandInTransaction<T> : Command<T>
    {
        public Func<T> Action { get; set; } 
        public CommandInTransaction(Func<T> actionToExecute)
        {
            Action = actionToExecute;
        }

        public override T Execute()
        {
            using (var transactionScope = new TransactionScope())
            {
                try
                {
                    var result = Action();
                    transactionScope.Complete();
                    return result;
                }
                catch (Exception)
                {
                    transactionScope.Dispose();
                    throw;
                }
            }
        }
    }
}