﻿(function ($, app) {
    app.MeetingViewModel = (function () {

        //===================================================
        // Dom changes and event handlers
        //===================================================
        var availableUsers = ko.observableArray([]);
        var participants = ko.observableArray([]);

        var meetingModel = ko.observable({
            subject: ko.observable(''),
            description: ko.observable(''),
            meetingTime: ko.observable('')
        });

        var addUserEventHandler = function(obj, element) {
            availableUsers.remove(function(item) {
                return item.Id == obj.Id;
            });

            participants.push(obj);
        };

        var removeUserEventHandler = function(obj, element) {
            participants.remove(function(item) {
                return item.Id == obj.Id;
            });

            availableUsers.push(obj);
        };

        var initAvailableUsers = function(available) {
            availableUsers.removeAll();
            _.each(available, function(item) {
                availableUsers.push(item);
            });
        };

        var initParticipants = function (part) {
            participants.removeAll();
            _.each(part, function(item) {
                participants.push(item);
            });
        };

        var saveMeeting = function() {
            var model = meetingModel();
            model.participants = participants();
            model.availableUsers = availableUsers();
            

            _.each(availableUsers(), function(item, index) {
                $('<input type="hidden" name=availableUsers[' + index + '].Id value = "' + item.Id + '"/>').appendTo($('#meetingForm'));
                $('<input type="hidden" name=availableUsers[' + index + '].Name value = "' + item.Name + '"/>').appendTo($('#meetingForm'));
            });
            _.each(participants(), function (item, index) {
                $('<input type="hidden" name=participants[' + index + '].Id value = "' + item.Id + '" />').appendTo($('#meetingForm'));
                $('<input type="hidden" name=participants[' + index + '].Name value = "' + item.Name + '"/>').appendTo($('#meetingForm'));
                $('<input type="hidden" name=participants[' + index + '].IsCreator value = "' + item.IsCreator + '"/>').appendTo($('#meetingForm'));
            });

            return true;
        };

        var notifyParticipants = function() {
            var message = $('#message').val();
            var meetingId = $('#Id').val();
            app.NotificationViewModel.sendMessage(meetingId, message);
        };

        return {
            participants: participants,
            availableUsers: availableUsers,
            addUserEventHandler: addUserEventHandler,
            removeUserEventHandler: removeUserEventHandler,
            initParticipants: initParticipants,
            initAvailableUsers: initAvailableUsers,
            meetingModel: meetingModel,
            saveMeeting: saveMeeting,
            notifyParticipants: notifyParticipants
        };
    }());

})(jQuery, window.app = window.app || {});