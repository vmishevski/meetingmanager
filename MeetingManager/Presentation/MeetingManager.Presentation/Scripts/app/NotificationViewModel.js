﻿(function($, app) {
    app.NotificationViewModel = (function() {
        var notificationHubProxy = $.connection.notificationHub;
        var userName;

        notificationHubProxy.client.displayMessage = function (message) {
            alert(message);
        };

        var init = function (userNameParam) {
            userName = userNameParam;
            $.connection.hub.start()
                .done(function() {
                    console.log('connected to server');
                    notificationHubProxy.server.joinGroups(userName);
                })
                .fail(function() {
                    console.log('connection failed');
                });
        };

        var notifyCreateMeeting = function(meetingId) {
            $.connection.hub.start()
                .done(function() {
                    notificationHubProxy.server.onNewGroupCreated(meetingId);
                })
                .fail(function() {
                    console.log('connection failed');
                });
        };

        var sendMessage = function(meetingId, message) {
            notificationHubProxy.server.sendNotification(meetingId, message);
        };

        return {
            init: init,
            notifyCreateMeeting: notifyCreateMeeting,
            sendMessage: sendMessage
        };
    }());    
})(jQuery, window.app = window.app || {});