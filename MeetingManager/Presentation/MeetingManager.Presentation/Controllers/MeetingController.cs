﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MeetingManager.Common.EntityModel;
using MeetingManager.Domain.Services;
using MeetingManager.Presentation.Models;
using MeetingManager.Presentation.Models.Mappings;

namespace MeetingManager.Presentation.Controllers
{
    public class MeetingController : BaseController
    {
        private readonly IMeetingService _meetingService;

        public MeetingController(IMeetingService meetingService)
        {
            _meetingService = meetingService;
        }

        /// <summary>
        /// Renders view with all meetings.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (GetCurrentUser() == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var meetings = _meetingService.GetAllMeetings()
                .Select(meeting => new MeetingListModel()
                                   {
                                       Creator = meeting.Creator.Name,
                                       MeetingTime = meeting.MeetingTime.ToString("g"),
                                       ParticipantsCount = meeting.Participants == null ? 0 : meeting.Participants.Count,
                                       Subject = meeting.Subject,
                                       Id = meeting.Id
                                   });



            return View(meetings);
        }

        /// <summary>
        /// Renders empty view for creating new meeting
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            var currentUser = GetCurrentUser();

            if (currentUser == null) return RedirectToAction("Index", "Home");
            
            ViewBag.IsCreate = true;
            var availableUsers =
                _meetingService.GetAllUsers()
                    .Where(user => user.Name != currentUser.Name)
                    .Select(user => new ParticipantModel() {Id = user.Id, Name = user.Name})
                    .ToList();

            currentUser.IsCreator = true;
            
            var model = new MeetingModel
                        {
                            AvailableUsers = availableUsers,
                            Participants = new List<ParticipantModel> {currentUser}
                        };

            return View("EditMeeting", model);
        }

        /// <summary>
        /// Retrieves the meeting by it's id and renders the view for editing that meeting
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var currentUser = GetCurrentUser();
            if (currentUser == null) return RedirectToAction("Index", "Home");

            var meeting = _meetingService.GetMeetingById(id);

            if (meeting == null) return RedirectToAction("Index");

            var model = meeting.ToModel();
            var participantIds = model.Participants.Select(participantModel => participantModel.Id).ToList();
            model.AvailableUsers =
                _meetingService.GetAllUsers()
                    .Where(user => !participantIds.Contains(user.Id))
                    .Select(Mapper.Map<ParticipantModel>)
                    .ToList();

            return View("EditMeeting", model);
        }

        /// <summary>
        /// Saves the meeting which was edited in the view
        /// If there are errors renders same view with the errors
        /// </summary>
        /// <param name="meeting"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Save(MeetingModel meeting)
        {
            ViewBag.IsCreate = ViewBag.IsCreate;
            if (!ModelState.IsValid)
            {
                ViewBag.IsCreate = true;
                return View("EditMeeting", meeting);
            }

            if (meeting.Participants == null || meeting.Participants.Count < 2)
            {
                ModelState.AddModelError("Participants", "Meeting must have at least two participants");
                return View("EditMeeting", meeting);
            }

            var currentUser = GetCurrentUser();

            var entity = meeting.ToEntity();

            entity.Creator = _meetingService.GetUserById(currentUser.Id);
            entity.Participants = new List<User>();
            foreach (var participantModel in meeting.Participants)
            {
                entity.Participants.Add(_meetingService.GetUserById(participantModel.Id));
            }

            var saved = _meetingService.SaveMeeting(entity);

            return View("SuccessCreateMeeting", saved.Id);
        }

        /// <summary>
        /// Delete single meeting from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            _meetingService.RemoveMeeting(id);

            return RedirectToAction("Index");
        }

	}
}