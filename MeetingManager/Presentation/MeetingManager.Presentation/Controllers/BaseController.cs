﻿using System;
using System.Web.Mvc;
using MeetingManager.Common.EntityModel;
using MeetingManager.Presentation.Models;

namespace MeetingManager.Presentation.Controllers
{
    public class BaseController : Controller
    {
        private const string UserKey = "user";
        protected ParticipantModel SaveCurrentUser(User user)
        {
            var participantModel = new ParticipantModel() {Id = user.Id, Name = user.Name};
            Session[UserKey] = participantModel;
            return participantModel;
        }

        protected ParticipantModel GetCurrentUser()
        {
            return Session[UserKey] as ParticipantModel;
        }

	}
}