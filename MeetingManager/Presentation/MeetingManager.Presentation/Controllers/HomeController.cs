﻿using System.Web.Mvc;
using MeetingManager.Domain.Services;

namespace MeetingManager.Presentation.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IMeetingService _meetingService;

        public HomeController(IMeetingService meetingService)
        {
            _meetingService = meetingService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogUser(string username)
        {
            if (!ModelState.IsValid)
            {
                return View("Index");
            }

            var user = _meetingService.AddUser(username);

            SaveCurrentUser(user);

            return RedirectToAction("Index", "Meeting");
        }
    }
}