﻿using System.Web.Mvc;

namespace MeetingManager.Presentation.Controllers
{
    public class ErrorController : BaseController
    {
        //
        // GET: /Error/
        public ActionResult Index()  
        {
            return View("Error");
        }
	}
}