﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using StructureMap;

namespace MeetingManager.Presentation.DependencyInjection
{
    namespace MeetingManager.Presentation.DependencyInjection
    {
        public class MeetingControllerFactory : DefaultControllerFactory
        {
            protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
            {
                if (controllerType == null)
                {
                    return base.GetControllerInstance(requestContext, controllerType);
                }
                return (Controller)ObjectFactory.GetInstance(controllerType);
            }

        }
    }
}