﻿using MeetingManager.Domain.DependencyInjection;
using StructureMap;

namespace MeetingManager.Presentation.DependencyInjection
{
    public static class ContainerBootstrap
    {
        /// <summary>
        /// Defines the dependency graph
        /// </summary>
        public static void Bootstrap()
        {
            ObjectFactory.Initialize(
                x => {
                         DomainIoC.Bootstrap(x);
                });
        }
    }
}