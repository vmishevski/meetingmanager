﻿using System;

namespace MeetingManager.Presentation.Models
{
    public class MeetingListModel
    {
        public int Id { get; set; }

        public string Subject { get; set; }

        public string MeetingTime { get; set; }

        public string Creator { get; set; }

        public int ParticipantsCount { get; set; }

    }
}