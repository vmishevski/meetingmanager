﻿using AutoMapper;
using MeetingManager.Common.EntityModel;

namespace MeetingManager.Presentation.Models.Mappings
{
    public static class AutoMappings
    {
        public static void Configure()
        {
            Mapper.CreateMap<User, ParticipantModel>();
            Mapper.CreateMap<ParticipantModel, User>();

            Mapper.CreateMap<Meeting, MeetingListModel>();
            Mapper.CreateMap<Meeting, MeetingModel>();
        }
    }
}