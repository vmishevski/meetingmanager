﻿using System;
using System.Linq;
using AutoMapper;
using MeetingManager.Common.EntityModel;

namespace MeetingManager.Presentation.Models.Mappings
{
    public static class MeetingModelMapping
    {
        public static Meeting ToEntity(this MeetingModel meetingModel)
        {
            return new Meeting()
                         {
                             Id = meetingModel.Id,
                             Subject = meetingModel.Subject,
                             MeetingTime = meetingModel.MeetingTime.HasValue ? meetingModel.MeetingTime.Value : DateTime.Now,
                             Description = meetingModel.Description,
                             Active = true
                         };
        }

        public static MeetingModel ToModel(this Meeting entity)
        {
            var model = new MeetingModel()
                   {
                       Id = entity.Id,
                       MeetingTime = entity.MeetingTime,
                       Description = entity.Description,
                       Subject = entity.Subject,
                       CreatorId = entity.Creator.Id
                   };

            model.Participants = entity.Participants
                .Select(user =>
                        {
                            var participant = Mapper.Map<ParticipantModel>(user);
                            participant.IsCreator = participant.Id == model.CreatorId;

                            return participant;
                        })
                .ToList();

            return model;
        }
    }
}