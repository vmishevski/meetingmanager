﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MeetingManager.Presentation.Models
{
    public class MeetingModel
    {
        public int Id { get; set; }

        [DisplayName("Subject")]
        [Required]
        public string Subject { get; set; }

        public int CreatorId { get; set; }

        public string Description { get; set; }

        [DisplayName("Meeting time")]
        [Required]
        public DateTime? MeetingTime { get; set; }

        [Required]
        public List<ParticipantModel> Participants { get; set; }

        public List<ParticipantModel> AvailableUsers { get; set; }

    }
}