﻿using System.ComponentModel.DataAnnotations;

namespace MeetingManager.Presentation.Models
{
    public class ParticipantModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public bool IsCreator { get; set; }

    }
}