﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using MeetingManager.Common.EntityModel;
using MeetingManager.Presentation.DependencyInjection;
using MeetingManager.Presentation.DependencyInjection.MeetingManager.Presentation.DependencyInjection;
using MeetingManager.Presentation.Models;
using MeetingManager.Presentation.Models.Mappings;

namespace MeetingManager.Presentation
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ContainerBootstrap.Bootstrap();
            ControllerBuilder.Current.SetControllerFactory(new MeetingControllerFactory());

            AutoMappings.Configure();
            
        }
    }
}
