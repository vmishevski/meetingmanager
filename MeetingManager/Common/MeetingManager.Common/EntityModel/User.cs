﻿using MeetingManager.Common.EntityModel.Base;

namespace MeetingManager.Common.EntityModel
{
    /// <summary>
    /// Defines user of our application
    /// </summary>
    public class User : BaseEntity
    {
        public string Name { get; set; }
    }
}
