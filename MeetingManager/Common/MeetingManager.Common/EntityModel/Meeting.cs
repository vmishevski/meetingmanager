﻿using System;
using System.Collections.Generic;
using MeetingManager.Common.EntityModel.Base;

namespace MeetingManager.Common.EntityModel
{
    public class Meeting : BaseEntity
    {
        public string Subject { get; set; }

        public string Description { get; set; }

        public DateTime MeetingTime { get; set; }

        public virtual ICollection<User> Participants { get; set; }

        public virtual User Creator { get; set; }

        public Boolean Active { get; set; }

        public void DoSomeStuff()
        {
            
        }
    }
}
