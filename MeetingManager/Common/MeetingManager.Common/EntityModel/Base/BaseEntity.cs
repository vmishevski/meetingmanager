﻿using System;

namespace MeetingManager.Common.EntityModel.Base
{
    /// <summary>
    /// Defines the base entity which contains all shared properties of all entities
    /// </summary>
    public class BaseEntity
    {
        public int Id { get; set; }

        public DateTime? UpdateDateTime { get; set; }

    }
}
